<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('soft_delete' ,'ORMController@softdelete');
Route::get('delete' ,'ORMController@delete');
Route::get('show_all' ,'ORMController@show_deleted_too');
Route::get('show_deleted_only' ,'ORMController@show_deleted_only');
Route::get('restore' ,'ORMController@restore');
Route::post('unique' ,'ORMController@create');
