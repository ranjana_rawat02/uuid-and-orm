<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Uuids;

use Webpatser\Uuid\Uuid;

class UniqueIdModel extends Model
{
    
    public static function boot()
    {   
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = (string) Uuid::generate(4);
        });
    }   

   public function add_user()
   {
    $user = new UniqueIdModel;
    $user->name = 'ranjana';
    $user->email = 'ranjana@gmail.com';
    $user->password = '123456'; 
    $user->save();
   }
}

